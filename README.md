# bash scripts for zcashd and bitcoind deployments

These bash scripts can be used to quickly build a new VPS running testnet bitcoind and zcashd. Useful for e.g. XCATs. I included the dependencies for Monero, because you never know when you might need them. Zcash has weird breaking problems if you don't have all the right crypto libraries in place. Don't forget to check that the target binaries to be downloaded are the most recent version. These things change all the time, and you could accidentally download and old version. You'll also want to change the rpcpassword for both .conf files the script creates.

The scripts are configured for x86_64. I left a snippet in there which can be used to install the debian tarball for zcash.  

Run `./rsync-upload.sh` on your local machine, remember to change the target remote IP address to yours. 

Then SSH into your remote machine and run `./install.sh`

For some reason there is no cli wallet functionality in zcash. I have no idea why, it is not in either tarball (debian/generic), and I cannot build from source due to an out of memory/recursion problem.