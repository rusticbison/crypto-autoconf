#!/bin/bash

#local machine
SOURCEDIR="/Users/rusticbison/local_documents/sandbox/scripts/crypto-autoconf"
TARGETDIR="~"

#destination machine: Ubuntu 18.04 x64
DEST="188.166.80.32"
DESTDIR=root@$DEST:$TARGETDIR

#upload local director crypto-autoconf to remote
rsync -avz --exclude="*.DS_Store" $SOURCEDIR $DESTDIR || { echo 'Upload failed :(' ; exit 1; }
ssh root@$DEST sudo chmod 700 -R $TARGETDIR/crypto-autoconf