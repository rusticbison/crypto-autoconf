#!/bin/bash

#install system dependencies
./install-system-dependencies.sh

SOURCE="https://bitcoin.org/bin/bitcoin-core-0.17.0.1/bitcoin-0.17.0.1-x86_64-linux-gnu.tar.gz"
FILENAME="bitcoin-0.17.0.1-x86_64-linux-gnu.tar.gz"
NAME="bitcoin-0.17.0"
INSTALLDIR="/usr/local/bin"
DIR="/root/.bitcoin"
CONF="bitcoin.conf"
TYPE="bitcoind"
CLI="bitcoin"

#Downloading bitcoin from $SOURCE"
wget $SOURCE

#Extract to $INSTALLDIR
tar -xvzf $FILENAME -C $INSTALLDIR

#Remove tarball
rm $FILENAME

#Fetch params (Zcash only)

#create a new config file if needed 
echo "Check to see if the $CLI directory exists..."
if [ ! -d $DIR ]; then
    echo "$DIR directory does not exist, creating..."
    mkdir $DIR || { echo "creating $CONF failed :(" ; exit 1; }
fi
echo "$DIR exists. Checking to see if $CONF is inside..."

if [ -f $DIR/$CONF ]; then
    rm -rf $DIR/$CONF
    echo "Creating $CONF..."
    touch $DIR/$CONF || { echo "creating $CONF failed :(" ; exit 1; }
fi

#Edit config file
echo "Updating $CONF..."
echo "testnet=1" >> $DIR/$CONF
echo "server=1" >> $DIR/$CONF
echo "prune=550" >> $DIR/$CONF
echo "rpcuser=user" >> $DIR/$CONF
echo "rpcpassword=password" >> $DIR/$CONF
echo "addnode=testnet.z.cash" >> $DIR/$CONF

#Add to $PATH
echo "export PATH=\"$INSTALLDIR/$NAME/bin:\$PATH\"" >> /root/.bashrc 
# source ~/.bashrc

#Run
echo "Starting $CLI daemon..."
$INSTALLDIR/$NAME/bin/$TYPE --daemon