#!/bin/sh

#install dependencies
sudo apt update && sudo apt install build-essential cmake pkg-config libboost-all-dev libssl-dev libzmq3-dev libunbound-dev libsodium-dev libunwind8-dev liblzma-dev libreadline6-dev libldns-dev libexpat1-dev doxygen graphviz libpgm-dev libc6-dev m4 g++-multilib autoconf libtool ncurses-dev unzip git python python-zmq zlib1g-dev wget curl bsdmainutils automake -y

#udpate system
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

#autoremove 
sudo apt autoremove -y