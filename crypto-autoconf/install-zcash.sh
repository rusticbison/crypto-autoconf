#!/bin/bash

#install system dependencies
./install-system-dependencies.sh

SOURCE="https://z.cash/downloads/zcash-2.0.1-linux64.tar.gz"
FILENAME="zcash-2.0.1-linux64.tar.gz"
NAME="zcash-2.0.1"
INSTALLDIR="/usr/local/bin"
DIR="/root/.zcash"
CONF="zcash.conf"
TYPE="zcashd"
CLI="zcash"

#use for debian installations
# echo "deb [arch=amd64] https://apt.z.cash/ jessie main" >> /etc/apt/sources.list.d/zcash.list
# sudo apt-get update && sudo apt-get install zcash -y
# zcash-fetch-params


#Downloading zcash from $SOURCE"
wget $SOURCE
#Extract to $INSTALLDIR
tar -xvzf $FILENAME -C $INSTALLDIR
#Remove tarball
rm $FILENAME
#Fetch params
$INSTALLDIR/$NAME/bin/zcash-fetch-params

#create a new config file if needed 
echo "Check to see if the $CLI directory exists..."
if [ ! -d $DIR ]; then
    echo "$DIR directory does not exist, creating..."
    mkdir $DIR || { echo "creating $CONF failed :(" ; exit 1; }
fi
echo "$DIR exists. Checking to see if $CONF is inside..."

if [ -f $DIR/$CONF ]; then
    rm -rf $DIR/$CONF
    echo "Creating $CONF..."
    touch $DIR/$CONF || { echo "creating $CONF failed :(" ; exit 1; }
fi

#Edit config file
echo "Updating $CONF..."
echo "testnet=1" >> $DIR/$CONF
echo "server=1" >> $DIR/$CONF
echo "prune=550" >> $DIR/$CONF
echo "rpcuser=user" >> $DIR/$CONF
echo "rpcpassword=password" >> $DIR/$CONF
echo "addnode=testnet.z.cash" >> $DIR/$CONF

#Add to $PATH
echo "export PATH=\"$INSTALLDIR/$NAME/bin:\$PATH\"" >> /root/.bashrc 
# source ~/.bashrc

#Run
echo "Starting $CLI daemon..."
$INSTALLDIR/$NAME/bin/$TYPE --daemon